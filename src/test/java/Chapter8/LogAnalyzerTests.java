package Chapter8;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzerTests {
    @Test
    public void semanticsChange(){
        LogAnalyzer logan = new LogAnalyzer();
        Assert.assertTrue(logan.isValid("abc"));
    }

    @Test
    public void afterSemanticsChange(){
        LogAnalyzer logan = new LogAnalyzer();
        logan.initialize();
        Assert.assertTrue(logan.isValid("abc"));
    }

    //Более удобная версия этого теста
    @Test
    public void goodTestSemanticsChange(){
        LogAnalyzer logan = makeDefaultAnlyzer();
        Assert.assertFalse(logan.isValid("abc"));
    }
    //Используется фабричный метод
    public static LogAnalyzer makeDefaultAnlyzer(){
        LogAnalyzer analyzer = new LogAnalyzer();
        analyzer.initialize();
        return analyzer;
    }

    //------------------------------------------------

}
