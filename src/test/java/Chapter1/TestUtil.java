package Chapter1;

import org.junit.Test;

public class TestUtil {
    public static void showProblem(String test, String message) {
        String msg = "---{0}---" +
                "{1}" +
                "------------------\n" + test + "\n" + message;
        System.out.println(msg);
    }

    @Test
    public void testReturnsZeroWhenEmptyString() {
        String testName = "testReturnsZeroWhenEmptyString";
        try {
            SimpleParser p = new SimpleParser();
            int result = p.parseAndSum("");
            if (result != 0) {
                showProblem(testName, "parseAndSum должен вернуть 0 для пустой строки");
            }
        } catch (Exception e) {
            showProblem(testName, e.toString());
        }
    }
}