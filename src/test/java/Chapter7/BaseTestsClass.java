package Chapter7;
import org.junit.After;

import static org.mockito.Mockito.*;
//Код после рефакторинга
public class BaseTestsClass {
    //Выделяем Общий служебный метод, который будет использоваться в производных классах
    public ILogger fakeTheLogger(){
        LoggingFacility.logger = mock(ILogger.class);
        return LoggingFacility.logger;
    }

    @After
    public void after()
    {
        // Необходимо сбрасывать статический ресурс между тестами
        LoggingFacility.logger = null;
    }


}
