package Chapter7;

import org.junit.After;
import org.junit.Test;

public class LogAnalyzerTests {
    @Test
    public void analyze_EmptyFile_ThrowsException(){
        LogAnalyzer logan = new LogAnalyzer();
        logan.analyze("myemptyfile.txt");
        //Остальная часть теста
    }

    @After
    public void after()
    {
        // Необходимо сбрасывать статический ресурс между тестами
        LoggingFacility.logger = null;
    }
}
