package Chapter4;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzerTests {
//    @Test
//    public void analyze_TooShortFileName_CallsWebService() {
//        FakeWebService mockService = new FakeWebService();
//
//        LogAnalyzer log = new LogAnalyzer(mockService);
//
//        String tooShortFileName = "abc.ext";
//        log.analyze(tooShortFileName);
//
//        Assert.assertEquals("Слишком короткое имя файла: abc.ext", mockService.lastError);
//    }

        @Test
    public void analyze_WebServiceThrows_SendEmail() {
        FakeWebService stubService = new FakeWebService();
        stubService.toThrow = new Exception("fake exception");

        FakeEmailService mockEmail = new FakeEmailService();

        LogAnalyzer log = new LogAnalyzer(stubService, mockEmail);

        String tooShortFileName = "abc.ext";
        log.analyze(tooShortFileName);

        Assert.assertEquals("someone@someone.ru", mockEmail.to);
        Assert.assertEquals("fake exception", mockEmail.body);
        Assert.assertEquals("can't log", mockEmail.subject);
    }
}