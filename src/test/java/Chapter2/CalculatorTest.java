package Chapter2;
import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void sum_ByDefault_ReturnsZero() {
        Calculator calc = makeCalc();
        int lastSum = calc.sum();

        Assert.assertEquals(0, lastSum);
    }

    @Test
    public void add_WhenCalled_ChangesSum() {
        Calculator calc = makeCalc();
        calc.add(1);
        int sum = calc.sum();

        Assert.assertEquals(1, sum);
    }

    private static Calculator makeCalc() {
        return new Calculator();
    }
}
