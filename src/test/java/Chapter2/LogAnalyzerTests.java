package Chapter2;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class LogAnalyzerTests {
    @Test
    public void isValidLogFileName_BadExtension_ReturnsFalse() {
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName("filewithbadextention.foo");

        Assert.assertFalse(result);
    }

    @Test
    public void isValidLogFileName_GoodExtensionLowercase_ReturnsTrue() {
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName("filewithgoodextention.slf");

        Assert.assertTrue(result);
    }

    @Test
    public void isValidLogFileName_GoodExtensionUppercase_ReturnsTrue() {
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName("filewithgoodextention.SLF");

        Assert.assertTrue(result);
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void isValidLogFileName_EmptyFileName_Throws() {
        LogAnalyzer analyzer = new LogAnalyzer();
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Имя файла должно быть задано");
        analyzer.isValidLogFileName("");
    }

    @Test
    public void isValidLogFileName_WhenCalled_ChangesWasLastFileNameValid() {
        LogAnalyzer analyzer = new LogAnalyzer();

        analyzer.isValidLogFileName("badname.foo");

        Assert.assertFalse(analyzer.wasLastFileNameValid);
    }
}
