package Chapter3;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzerTests {
    @Test
    public void isValidLogFileName_NameSupportedExtension_ReturnsTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.willBeValid = true;

        ExtensionManagerFactory.setManager(myFakeManager);
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName("short.ext");

        Assert.assertTrue(result);
    }

    @Test
    public void isValidLogFileName_ExtManagerThrowsException_ReturnsFalse() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.willThrow = new Exception("Это подделка");
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName("anything.anyext");

        Assert.assertFalse(result);
    }

    @Test
    public void overrideTest() {
        FakeExtensionManager stub = new FakeExtensionManager();
        stub.willBeValid = true;

        TestableLogAnalyzer logan = new TestableLogAnalyzer(stub);

        boolean result = logan.isValidLogFileName("file.ext");

        Assert.assertTrue(result);
    }
}
