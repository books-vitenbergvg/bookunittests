package Chapter5;

import com.sun.org.apache.xpath.internal.Arg;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


public class LogAnalyzerTests {
    //Mockery context = new JUnit4Mockery();

//    @Test
//    public void Analyze_TooShortFileName_CallLogger() throws Exception {
//        final ILogger logger = context.mock(ILogger.class);
//        Chapter4.LogAnalyzer analyzer = new Chapter4.LogAnalyzer(logger);
//        analyzer.minNameLength = 6;
//        analyzer.analyze("a.txt");
//        context.checking(new Expectations() {{
//            oneOf(logger).logError("Слишком короткое имя файла: a.txt");
//        }});
//    }

    @Test
    public void returns_ByDefault_WorksForHardCodedArgument() {
        IFileNameRules fakeRules = mock(IFileNameRules.class);
        when(fakeRules.isValidLogFileName("strict.txt")).thenReturn(true);
        Assert.assertTrue(fakeRules.isValidLogFileName("strict.txt"));
    }

    @Test
    public void returns_ByDefault_WorksForAnyArgument() {
        IFileNameRules fakeRules = mock(IFileNameRules.class);
        when(fakeRules.isValidLogFileName(anyString())).thenReturn(true);
        Assert.assertTrue(fakeRules.isValidLogFileName("strict.txt"));
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void returns_ArgAny_Throws() {
        IFileNameRules fakeRules = mock(IFileNameRules.class);
        exception.expect(Exception.class);
        exception.expectMessage("Имя файла должно быть задано");
        doThrow(new Exception("fake exception")).when(fakeRules).isValidLogFileName(anyString());
        fakeRules.isValidLogFileName(anyString());
    }

    //Без использования jMock
    @Test
    public void analyze_LoggerThrows_CallWebService() {
        IWebService mockWebService = new FakeWebService();
        FakeLogger stubLogger = new FakeLogger();
        stubLogger.willThrow = new Exception("fake exception");

        LogAnalyzer analyzer = new LogAnalyzer(stubLogger, mockWebService);
        analyzer.minNameLength = 8;
        String tooShortFileName = "abc.ext";
        analyzer.analyze(tooShortFileName);
        Assert.assertTrue(((FakeWebService) mockWebService).messageToWebService.contains("fake exception"));
    }

    //С использованием jMock ??
    @Test
    public void analyze_LoggerThrows_CallWebServiceWithJMock() throws Exception {
        IWebService mockWebService = mock(IWebService.class);
        ILogger stubLogger = mock(ILogger.class);
        //doThrow(new Exception("fake exception")).when(stubLogger.logError(anyString()));//?

        LogAnalyzer analyzer = new LogAnalyzer(stubLogger, mockWebService);
        analyzer.minNameLength = 10;
        analyzer.analyze("Short.txt");
        verify(mockWebService).write(contains("fake exception"));
    }
}
