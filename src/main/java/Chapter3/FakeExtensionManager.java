package Chapter3;

public class FakeExtensionManager implements IExtensionManager {
    public boolean willBeValid = false;
    public Exception willThrow = null;

    public boolean isValid(String fileName) {
        if(willThrow != null){
            try {
                throw willThrow;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return willBeValid;
    }
}
