package Chapter3;

import Chapter3.FakeExtensionManager;
import Chapter3.IExtensionManager;

public class ExtensionManagerFactory {
    private static IExtensionManager customManager = null;

    public static IExtensionManager create() {
        if (customManager != null) {
            return customManager;
        }
        return new FakeExtensionManager();
    }

    public static void setManager(IExtensionManager mgr){
        customManager = mgr;
    }
}
