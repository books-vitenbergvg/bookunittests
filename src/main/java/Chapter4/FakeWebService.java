package Chapter4;

public class FakeWebService implements IWebService {
//    public String lastError;
//
//    public void logError(String message){
//        lastError = message;
//    }

    public Exception toThrow;

    public void logError(String message) throws Exception {
        if (toThrow != null) {
            throw toThrow;
        }
    }
}
