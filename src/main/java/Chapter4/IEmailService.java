package Chapter4;

public interface IEmailService {
    void sendMail(String to, String subject, String body);
}
