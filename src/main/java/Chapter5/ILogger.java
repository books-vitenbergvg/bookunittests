package Chapter5;

public interface ILogger {
    void logError(String message) throws Exception;
}
