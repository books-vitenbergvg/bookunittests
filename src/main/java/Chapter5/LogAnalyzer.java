package Chapter5;

import org.apache.commons.io.FilenameUtils;

public class LogAnalyzer {
    private ILogger _logger;
    private IWebService _webService;

    public LogAnalyzer(ILogger logger, IWebService webService) {
        _logger = logger;
        _webService = webService;
    }

    public int minNameLength;

    public void analyze(String fileName) {
        if (fileName.length() < minNameLength) {
            try {
                _logger.logError(String.format("Слишком короткое имя файла: {0}", fileName));
            } catch (Exception e) {
               _webService.write("Ошибка регистратора " + e);
            }
        }
    }


    public boolean wasLastFileNameValid;

    public boolean isValidLogFileName(String fileName) {
        wasLastFileNameValid = false;
        if (fileName == "") {
            throw new IllegalArgumentException("Имя файла должно быть задано");
        }
        if (!FilenameUtils.getExtension(fileName).equalsIgnoreCase("SLF")) {
            return false;
        }
        wasLastFileNameValid = true;
        return true;
    }
}
