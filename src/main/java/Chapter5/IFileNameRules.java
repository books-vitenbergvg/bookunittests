package Chapter5;

public interface IFileNameRules {
    boolean isValidLogFileName(String fileName);
}
