package Chapter2;

import org.apache.commons.io.FilenameUtils;

public class LogAnalyzer {
    public boolean wasLastFileNameValid;

    public boolean isValidLogFileName(String fileName) {
        wasLastFileNameValid = false;
        if (fileName == "") {
            throw new IllegalArgumentException("Имя файла должно быть задано");
        }
        if (!FilenameUtils.getExtension(fileName).equalsIgnoreCase("SLF")) {
            return false;
        }
        wasLastFileNameValid = true;
        return true;
    }
}
