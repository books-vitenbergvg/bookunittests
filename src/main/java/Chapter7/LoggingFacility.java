package Chapter7;

public class LoggingFacility
{
    public static ILogger logger;

    public static void log(String text)
    {
        logger.log(text);
    }
}
