package Chapter7;
//Игнорируется принцип переиспользования(внутри класса используется LoggingFacility)
public class LogAnalyzer {

    public void analyze(String fileName) {
        if (fileName.length() < 8) {
            LoggingFacility.log("Слишком короткое имя файла: " + fileName);
        }
    }
}
